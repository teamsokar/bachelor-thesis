\section{Klassische Eingabegeräte} 
\label{classicalInputDevices}
Die klassischen Eingabegeräte für PCs und Workstations bilden die als Standard geltende Maus und Tastatur. Sie bestimmten seit Jahrzehnten die Interaktion mit Computern.

\subsection{Tastatur}
\label{keyboard}
Die Tastatur dient zur Texteingabe. Sie ist seit Beginn des Informationszeitalters untrennbar mit Computern verbunden. Die Anordnung der Tasten basiert auf dem der ersten massenproduzierten, mechanischen Schreibmaschinen, die auf C.L. Sholes im Jahre 1868 zurückgehen \cite{TypeWritingPatent}. Der Fokus lag hier weniger auf der Ergonomie, sondern dass häufig aufeinanderfolgende Tasten weit auseinander liegen, damit sich die Typenhebel nicht verklemmen. Das daraus resultierende \textit{QWERTY}-Layout ist bis heute Standard und auf den meisten Tastaturen zu finden. Die bedeutenden Ausnahmen bilden Layouts mit kleinen, landestypische Änderungen wie das in Deutschland geläufige \textit{QWERTZ}. Unter Ergonomischen Gesichtspunkten entworfene Anordnungen wie das Dvorak-Layout \cite{cassingham1986dvorak} konnten sich nicht durchsetzen.\\
\cite[S. 10]{MenschMaschineKomm}

\subsection{Maus}
\label{mouse}
Die Anfang der 60er Jahre von Douglas Engelbart entwickelte Maus \cite{MousePatent} dient als indirektes Zeigegerät auf den Bildschirm. Bewegungen auf einer anderen Fläche, zum Beispiel dem Tisch, werden relativ auf den Bildschirm übertragen. Dazu wird die Unterlage abgetastet, entweder opto-mechanisch oder rein optisch.\\
Opto-mechanische Mäuse verwenden eine Kugel an der Unterseite, deren Drehung gemessen wird und entsprechend in Bewegung des Mauszeigers auf dem Bildschirm umgesetzt wird. Bei rein optischen Mäusen wird die Oberfläche mit einem Licht- oder Laserstrahl bestrahlt, eine einfache Digitalkamera erkennt die Änderungen der Oberfläche, \mbox{woraus} die relative Bewegung bestimmt wird.

\subsubsection{Trackball}
\label{trackball}
Eine sehr ähnliche Sonderform der Maus ist der sogenannten Trackball. Er funktioniert wie eine opto-mechanische Maus, allerdings ist die Kugel oben und größer, sie wird mit einem Finger direkt gedreht. Die Handhabung erfordert mehr Übung als bei der Maus, dafür wird weniger Fläche benötigt.

\subsubsection{Space Mouse}
\label{spacemouse}
Eine weitere Varianten, die speziell im CAD-Bereich weit verbreitet ist, ist die Space Mouse (teilweise auch 3D-Mouse genannt). Sie besteht aus einem Puck, der auf einem Standfuß befestigt ist. Der Puck kann in alle Richtungen geschoben, gezogen, gedreht und geneigt werden, wodurch alle Freiheitsgrade im dreidimensionalen Raum manipuliert werden können. Die Bewegung wird über Dehnungsmessstreifen bestimmt, nach einer Bewegung springt der Puck wieder in seine Ausgangsposition zurück.\\
\cite[S. 13-16]{MenschMaschineKomm}\\
Da die Space Mouse keinen Zeiger unterstützt, ist sie als Zusatzgerät zur Steuerung mit einer normalen Maus gedacht.\\
\cite[S. 87]{EmoInteraktionsDsgn}

\section{Stift}
\label{stylus}
Bei indirekten Eingabegeräten, wie der Maus, mussten die Handbewegungen an einem anderen Ort in Bewegungen auf dem Bildschirm umgewandelt werden. Dies führt zu einer höheren kognitiven Belastung für den Benutzer, da die physische Bewegung nicht eins zu eins der Umsetzung auf dem Bildschirm entspricht. Bei direkten Zeigegeräten ist dies nicht der Fall.\\
Ein erster Ansatz zur direkten Eingabe auf berührungsempfindlichen Bildschirmen sind Systeme mit einem speziellen Stift. Eine ähnliche Form der Eingabe bieten Touchscreens, siehe dazu Abschnitt \ref{touchscreen}. Beide Techniken haben ihre Vor- und Nachteile und werden je nach Anwendungsfall eingesetzt.\\
Beide gehören zu den direkten Eingabeformen, Stifte sind aber präziser und durch den dünnen Stift wird der Bildschirm weniger stark verdeckt. Sie dienen primär als Mausersatz. Neuere Geräte unterstützen sogenannten \textit{hover}-Gesten, die bereits erkannt werden, wenn der Stift dem Bildschirm nur nahe kommt und ihn nicht berührt. Ein großer Nachteil ist, dass ein zusätzliches Eingabegerät benötigt wird. Durch die kompakte Größe gehen die Stifte schnell verloren, wodurch das Gerät nur noch stark eingeschränkt oder komplett unbenutzbar wird.

\section{Touchscreen}
\label{touchscreen}
Der Touchscreen ist wie die Eingabe per Stift ein direktes Eingabegerät, allerdings erfolgt die Eingabe mit den Fingern, ein zusätzliches Gerät wird nicht benötigt. Zur Bestimmung der Position gibt es verschiedene Verfahren, die je nach Einsatzort gewählt werden. In den folgenden Abschnitten werden die am meisten eingesetzten Techniken kurz vorgestellt.

\subsection{Optischer Touchscreen}
Der optische Touchscreen basiert auf dem gleichen Prinzip wie eine Lichtschranke: Im Rand sind Leuchtdioden eingebaut, die für das menschliche Auge unsichtbares Licht aussenden. Gegenüber sind entsprechenden Empfänger, jeweils eine Reihe an einer der horizontalen und vertikalen Seiten. Dadurch wird ein Gitter aufgebaut, die Erkennung des Fingers oder Stifts wird aus der Unterbrechung der Lichtstrahlen ermittelt. Da keine Technik direkt auf dem Display benötigt wird, können diese Art der Touchscreens sehr robust gebaut werden und eigenen sich damit für öffentliche Einrichtungen, wie zum Beispiel Fahrkartenautomaten am Bahnhof.\\
\cite[S. 18]{MenschMaschineKomm}

\subsection{Kapazitiver Touchscreen}
An eine leitend beschichtete Glasplatte wird an jeder der vier Ecken eine Spannung angelegt. Wird sie mit einem Finger oder ladungsabsorbierendem Griffel berührt, entsteht eine kapazitive Koppelung. Dadurch fließt ein Strom ab, der aus den Teilströmen, die an den Ecken anliegen, gebildet wird. Die Position kann durch das Verhältnis der Ströme bestimmt werden. Es wird zur Benutzung zwingend ein ladungsabsorbierendes Gerät benötigt, wie spezielle Stifte oder Finger ohne Handschuhe.\\
\cite[S. 20]{MenschMaschineKomm}

\subsection{Resistiver Touchscreen}
Ein resistiver Touchscreen besteht aus drei Schichten: zwei leitenden, die durch eine isolierende getrennt sind. Die isolierende Schicht besteht aus sogenannten Isolatorpunkten, die bei Druckeinwirkung von Außen einen elektrischen Kontakt zwischen den leitenden Schichten herstellen. Dabei entsteht ein Spannungsteiler, bestehend aus jeweils zwei Widerständen an jeder der leitenden Schichten. Aus dem Verhältnis der jeweils zusammengehörigen Widerständen der Schichten lässt sich die Berührungsposition bestimmten.\\
Durch die häufigen Spannungswechsel bei Berührungen entsteht um einen resistiven Touchscreen ein elektromagnetisches Störfeld. Da der Kontakt durch Druck ausgelöst wird, wird kein bestimmtes Zeigegerät benötigt.\\
\cite[S. 19]{MenschMaschineKomm}

\section{Multitouch}
\label{multitouch}
Die bisherigen Touchscreens und Stifte zielten darauf ab, die Maus als Zeigegerät durch eine berührungsempfindliche Oberfläche zu ersetzten. Bei beiden wird nur ein Berührungspunkt erkannt, weitere Funktionalität war technisch nicht möglich oder nicht vorgesehen. Multitouch-fähige Oberflächen sind in der Lage, mehrere Berührungen gleichzeitig zu erkennen. Dadurch wird eine echte direkte Manipulation von Objekten möglich, die sonst über Stellvertreter wie den Mauszeiger realisiert wurde.\\
Die Berührung mit dem Finger benötigt keine weiteren Eingabegeräte, ist aber weniger präzise als eine Maus oder Stift. Zudem wird dadurch das anvisierte Objekt schnell verdeckt, wodurch feine Manipulationen erschwert werden. Wenn pixelgenaues Arbeiten erforderlich ist, ist ein Stellvertreter-Zeiger die bessere Form der Eingabe. Für dreidimensionale Objekte sollte eine alternatives Eingabegerät, wie zum Beispiel eine Space-Mouse (Abschnitt \ref{spacemouse}) in Betracht gezogen werden, da hiermit je nach Anwendung bessere Ergebnisse erzielt werden können.

\subsection{Gesten}
\label{gestures}
Durch Multitouch wurde es möglich, nicht nur durch Zeigen ein Gerät zu steuern, sondern zusätzlich sogenannten Gesten anzubieten, mit der bestimmte Aktion ausgelöst werden können. Diese Gesten sollen zur besseren Erlernbarkeit an Bewegungen in der realen Welt erinnern, was aber nicht immer möglich ist.\\
Ein gutes und bekanntes Beispiel ist die \textit{Spreizgeste} (engl. pinch), die als Vorreiter aller weiteren Gesten gilt. Dabei wird mit dem Daumen und dem Zeigefinger die Oberfläche berührt und die Finger auseinander oder zueinander bewegt. Ein häufig genutzt Anwendung ist das Vergrößern und Verkleinern eines Bildausschnittes, was so sehr intuitiv gelöst wurde.\\
Ein Beispiel für eine schlechte Umsetzbarkeit mit Gesten ist das Konzept einer Zwischenablage, wie sie am PC Gang und Gebe ist. Sie wird mit der Maus über das Kontextmenü oder per Tastaturkürzel aufgerufen. Beides steht auf einem Touchscreen nicht zur Verfügung, für die Lösung haben Apple und Microsoft unterschiedliche Wege eingeschlagen.\\
Microsoft legt für die grundlegende Interaktion acht sogenannten Schnippgesten (engl. flick) fest, bei der der Finger schnell in eine Richtung bewegt wird. Für jede Richtung ist ein anderes Verhalten festgelegt. Alle müssen erlernt werden und lassen kaum Bezüge zu realem Verhalten zu. Auch ist die Verwechslungsgefahr hoch, wenn man sich nur teilweise an eine Geste erinnert. Die Gesten zum Kopieren und Einfügen sind eine Bewegung nach rechts oben bzw. rechts unten. Für Übersicht siehe Abbildung \ref{fig:msFlick}, die durchgeführten Systemereignisse in Tabelle \ref{tab:msFlickSysEvents}.\\

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.3\textwidth]{gfx/ms_flick.png}
	\caption{Acht Schnippgesten in Windows 7 und Windows Phone.\\
	Quelle: \cite{MSTouchGuideline}}
	\label{fig:msFlick}
\end{figure}

\begin{table}
	\centering
	\begin{tabular}{| l | l |}
	\hline
	\textbf{Schnipprichtung} & \textbf{Systemereignis} \\ \hline
	links & Vorwärts \\ \hline
	rechts & Rückwärts \\ \hline
	hoch & Tastatur Scroll runter \\ \hline
	runter & Tastatur Scroll hoch \\ \hline
	links-hoch & Tastatur löschen \\ \hline
	links-runter & Tastatur Undo \\ \hline
	rechts-hoch & Tastatur kopieren \\ \hline
	rechts-runter & Tastatur einfügen \\
	\hline
	\end{tabular}
\caption{Schnippgesten in Windows 7 und Windows Phone und zugehörige Systemereignisse}
\label{tab:msFlickSysEvents}
\end{table}

Apple hat erst gar nicht versucht, eine spezielle Geste zu entwickeln. Stattdessen wurde das bekannte Kontextmenü integriert, das über eine sogenannten \textit{tap-and-hold}-Geste geöffnet wird.
%### bessere Formulierung
%Dabei verweilt der Finger kurz auf der Oberfläche, statt den Bildschirm nur kurz anzutippen.
Dabei verweilt der Finger kurz auf dem Bildschirm, statt ihn nur anzutippen.
%Statt den Bildschirm nur kurz zu berühren, verweilt der Finger kurz auf der Oberfläche.
%Dabei wird der Bildschirm kurz angetippt, aber nicht sofort losgelassen, der Finger verweilt kurz auf dem Bildschirm.
Zwar dauert diese Geste länger als die Lösung von Microsoft, da sie ein weiteres Menü öffnet in dem die Auswahl getroffen werden muss, aber sie hat eine geringere Verwechslungsgefahr. Außerdem fungiert die \textit{tap-and-hold}-Geste auf den meisten Systemen wie ein Rechtsklick mit der Maus, welche auf dem PC das Kontextmenü öffnet.\\
\cite{EmoInteraktionsDsgn}\\
\\
Multitouch gehört heute zum Standard für Touchscreen Bedienoberflächen und wurde deshalb berücksichtigt.

\section{Bewegungssteuerung}
\label{move}
Erstmals in die breite Masse brachte diese Art der Steuerung die Wii Spielekonsole von Nintendo. Die sogenannten \textit{Sensorleiste}, eine kleine Erweiterung der Konsole, die vor dem Fernseher platziert wird, erzeugt mit Infrarotstrahlen eine Gitter, in der die Wii-Remote genannten, an eine Fernbedienung erinnernden Controller geortet werden können. Dabei wird die Position im Raum sowie die Stellung des Controllers per Bluetooth an die Konsole übertragen. Dadurch lassen sich Bewegungen des ganzen Armes zur Steuerung nutzen, wodurch eine realitätsnähere Simulation zum Beispiel von Sportarten möglich ist. Die Wii-Remote dient dabei als Stellvertretergerät und hat zusätzliche Tasten zur weiteren Bedienung. Da nur dieses Gerät und nicht der Spieler selber erkannt wird, ist die Erkennungsfunktion eingeschränkt. Dafür muss nur wenig Rechenleistung für die Erkennung aufgewendet werden. Das funktionsfähige System wurde erstmals auf der \gls{e3} 2006 vorgestellt, verkauft wird die Konsole seit Ende 2006.\\
\cite{WikiWii}\\
\\
Fast drei Jahre später, im Juni 2009, zeigte Sony mit \textit{PlayStation Move} eine ähnliche Erweiterung für die PlayStation 3 Spielekonsole. Hier wird ebenfalls ein Controller als Stellvertreter genutzt, der vom Aussehen an ein Mikrofon erinnert und über weitere Tasten verfügt. Es gelten die selben Vor- und Nachteile wie bei Nintendos Wii. Die Erweiterung kam Anfang November 2010 in den Handel.\\
\cite{WikiPSMove}\\
\\
Einen anderen Weg schlug Microsoft ein. Um Bewegungsteuerung auf der XBox360 Spielekonsole zur ermöglichen wurde im Juni 2009 die Erweiterung \textit{Kinect} vorgestellt. In einem zusätzlichen Gerät sind zwei Kameras integriert, die die Spieler aufnehmen und dadurch erkennen sowie deren Position im Raum bestimmen können. Durch das Entfallen eines Stellvertretergerätes können Bewegungen des ganzen Körpers erfasst werden, zum Preis der verringerten Genauigkeit. Es können Arme, aber keine einzelnen Finger erkannt werden. Außerdem entsteht höherer Rechenaufwand, welcher vor allem auf die zusätzliche Bildverarbeitung zurückzuführen ist. Das System enthält zudem Mikrofone zur Spracherkennung, näheres dazu siehe Abschnitt \ref{speech}.\\
Neben der Erweiterung für die Spielekonsole gibt es auch eine Version für PCs mit USB-Anschluss statt eines proprietären und entsprechenden Treibern für Windows Betriebssysteme, sowie einem SDK. Es liefert zudem präzisere Positionsangaben, die sich im Millimeterbereich bewegen.\\
\cite{WikiKinect}

\section{Sprachsteuerung}
\label{speech}
Die Sprachsteuerung ist ein Teil der sogenannten Sprachkommunikation, welche beide die Interaktion zwischen Mensch und Maschine über die gesprochene Sprache vorsehen. Die Sprachkommunikation deckt alle Gebiete ab und zielt auf einen möglichst natürlich Umgang des Menschen mit der Maschine ab. Dazu muss die gesprochene Sprache vom Computer interpretiert werden. Dies gestaltet sich sehr schwierig, da die Stimme während des Sprechens leicht variiert. Zum Beispiel erhöht sich die Tonhöhe bei einer Frage leicht zum Ende des Satzes hin. Sprachfehler, Behinderungen oder Dialekte verschärfen das Problem weiter, dazu kommen noch Störungen durch Umgebungsgeräusche. Jede bekannte Sprachsteuerung muss deshalb vor dem Gebrauch trainiert werden, damit sie die Eigenarten des Sprechers berücksichtigen kann, wodurch oft wechselnde Nutzer aber ausgeschlossen sind.\\
Die Sprachsteuerung reduziert den Aufwand der Erkennung dadurch, das keine kompletten Sätze, sondern einzelne Kommandos genutzt werden. Diese lassen sich von der Software besser unterscheiden und trennen, da kein natürlicher Sprachfluss entsteht. Dies ist gleichzeitig der größte Nachteil bei der Bedienung, da es für den Nutzer ungewohnt ist, in einer Kommandosprache zu sprechen.\\
Ebenfalls zu diesem Gebiet gehört die Sprachsyntese, die zur Rückmeldung genutzt werden kann. Die Wörter werden dafür in ihre Laute zerlegt, die sogenannten \textit{Phoneme}. Diese werden von einem Sprecher eingesprochen und anschließend auf Basis eines geschrieben Textes genutzt, um Sprache zu erzeugen.\\
\cite[S. 123]{MenschMaschineKomm} / \cite[S. 370 - 375]{DixHCI}

\section{Einsatz im Shop\-floorprojekt}
Der Hauptfokus des Projektes liegt auf der Entwicklung eines Bedienkonzepts für Touchscreens. Alternativ soll eine Bedienung per Maus und Tastatur sowie per Stift möglich sein. Die Eingabe per Stift wird bereits vom \gls{dotnet} unterstützt und kann ohne zusätzlichen Entwicklungsaufwand genutzt werden.\\
Die Nutzung einer Bewegungssteuerung ohne Stellvertretergerät erscheint sehr interessant. Aus Zeitmangel konnte diese Eingabeform in dieser Arbeit nicht berücksichtigt werden. Sie wird in einem späteren Projekt weiter untersucht. Siehe dazu \cite{BscDani}.\\
Der Einsatz einer Sprachsteuerung würde für dieses Projekt zwar Vorteile bringen (u.a. Unabhängigkeit von Eingabegeräten), allerdings ist sie durch den Lärm in Fabrikhallen unpraktikabel und wurde deshalb nicht weiter berücksichtigt.
