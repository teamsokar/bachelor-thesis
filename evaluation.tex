Während der Entwicklung des Bedienkonzeptes wurden bereits kleine, informelle Evaluierungen durchgeführt. Die Ergebnisse sind in den ``Feedback''-Unterabschnitten der jeweiligen Iterationen zu finden, siehe dazu in Kapitel \ref{concept} die Abschnitte \ref{guided-measurement} und \ref{optimized-ui}. Nach Abschluss der Entwicklung des Konzeptes wurde eine größer angelegte und formalere Evaluierung durchgeführt. In diesem Kapitel werden die Evaluierungsmethoden, das Vorgehen und die Ergebnisse behandelt.

\section{Messaufgabe}
Für alle Evaluierungen in diesem Kapitel wird eine typischen Aufgabe benötigt, die durch die Tester mit den Prototyp gelöst werden soll. Eine solche Aufgabe im Werkstattbereich stellt das Messen von \gls{regelgeo}-Elementen und ihrer Lage zueinander dar. Konkret wurde deshalb das messen von zwei Kreisen sowie das bestimmen der minimalen Distanz dazwischen ausgewählt. Als Bedienkonzept wurde die Geführte Messung (Beschreibung in Abschnitt \ref{guided-measurement}) ausgewählt. Die Maße zur Eingabe werden von einer Zeichnung abgelesen und manuell eingeben, ein CAD-Modell ist nicht verfügbar. Zu Beginn ist kein Tastsystem konfiguriert. Soweit möglich werden voreingestellte Standardwerte (zum Beispiel bei der Anzahl der Messpunkte) übernommen. Zum Schluss soll ein einfaches Protokoll als pdf-Datei exportiert werden.\\
Die Aufgabe erscheint zwar sehr einfach und wenig, aber mehr Elemente würden zu keinen Zugewinn an Kenntnissen führen. Die Aufgabe wird zwar größer, aber die Komplexität ändert sich nicht.\\
Für die tabellarische Auflistung der Aufgaben siehe Tabelle \ref{tab:TaskListTwoCirclesMinDistance}.

\begin{table}
	\centering
	\begin{tabular}{| p{0.2\linewidth} | p{0.75\linewidth} |}
	\hline
	\textbf{Phase} & \textbf{Aufgabe} \\ \hline
	Vorbereitung & Einstellen des Tastsystems ``DuraMax'' \\ \hline
	& Einstellungen Manuelle Messung: Werkstoff beliebig, Temperatur von Sensor der Maschine, kein CAD-Modell \\ \hline
	Messung & Merkmale festlegen: keine Ausrichtung, zwei Kreise und minimale Distanz zwischen beiden Kreisen erstellen\\ \hline
	& Sollwerte und Toleranzen festlegen: Kreise: Koordinaten Mittelpunkt und Durchmesser, Werte beliebig. Distanz: Toleranz X/Y/Z und euklidische Distanz, Werte beliebig. \\ \hline
	& Messung starten: Punkte antasten, Darstellung als Animation \\ \hline
	Auswertung & Protokollstil bleibt Standard \\ \hline
	& Merkmale zum Protokoll hinzufügen \\ \hline
	& pdf-Export \\ \hline
	\end{tabular}
\caption{Aufgabeliste Messung zwei Kreise und minimale Distanz}
\label{tab:TaskListTwoCirclesMinDistance}
\end{table}


\section{Experten Analyse}
\label{eval_experts}
Da bisher nur eine Evaluierung mit Experten aus dem Bereich der Messtechnik erfolgte, nicht aber der Usability, wurde dafür eine eigene Teilevaluierung entworfen. Evaluiert wurde mit Mitarbeitern der Firma Intuity \cite{IntuityWebsite}, die in die Entwicklung von Bedienkonzepten, Oberflächen und Styleguides für anderen \gls{imt}-Produkte eingebunden ist. Zudem wurde mit Mitarbeitern der Carl Zeiss AG, die im Bereich Usability geschult sind und Erfahrung haben, evaluiert.\\
Als Evaluierungsmethoden wurden jeweils ein \textit{Cognitive Walkthrough} und eine \textit{Heuristic Evaluation} durchgeführt.

\subsection{Cognitive Walkthrough}
Der \textit{Cognitive Walkthrough} basiert auf \textit{Code Walkthroughs}, welche für die Software-Entwicklung entwickelt wurden. Dabei gehen Entwickler zusammen mit Experten durch die Abschnitte des Programm-Codes und analysieren ihn auf Schwachstellen und Probleme. Ähnlich verläuft ein \textit{Cognitive Walkthrough}, allerdings wird hierbei die Interaktion des Programms mit einem Prototyp oder Spezifikation des Systems durchlaufen. Für die Durchführung werden die folgenden Komponenten benötigt:

\begin{itemize}
	\item Spezifikation oder Prototyp des Systems.
	\item Eine typische Aufgabe, die ein Benutzer ausführt.
	\item Eine Liste von Aktionen, die ein Benutzer im System zur Erledigung der Aufgabe auszuführen hat.
	\item Beschreibung des Zielgruppen-Benutzers mit Angaben zu Vorkenntnissen.
\end{itemize}

Mit diesen Voraussetzungen gegeben gehen die Evaluatoren Schritt für Schritt durch das Programm, als Referenz die Aktionsliste. Damit soll das System evaluiert und eine Einschätzung über die Bedienbarkeit abgegeben werden. Dazu versuchen sie, bei jedem Schritt die folgenden Fragen zu beantworten:

\begin{enumerate}
	\item Stimmt der Effekt der Benutzeraktion mit dem Ziel des Benutzers zu diesem Zeitpunkt überein?
	\item Erkennt der Benutzer, dass die Aktion verfügbar ist?
	\item Wenn der Benutzer die Aktion gefunden hat, kann er erkennen, dass es sich um die Benötigte handelt?
	\item Verstehen die Benutzer die Rückmeldung nach Ausführung der Aktion?
\end{enumerate}

Wichtig ist an dieser Stelle die Dokumentation des Vorgangs, insbesondere von gefundenen Problemen. Daneben wurden die Revision des Prototyps sowie eine Einschätzung über den Schweregrad des Problems festgehalten.\\
\cite[S. 321 - 322]{DixHCI}

\subsection{Heuristic Evaluation}
In einer \textit{Heuristic Evaluation} wird ein Prototyp oder Spezifikation anhand von Regeln und Erfahrungswerten bewertet. Diese Art der Evaluierung kann bereits in frühen Entwicklungsstadien angewandt werden, bleibt aber während des gesamten Entwicklungsprozesses praktikabel. Ein Kernpunkt ist, dass mehrere Evaluatoren unabhängig voneinander ein System bewerten.\\
Als Kriterien eigenen sich, neben Erfahrungswerten und Faustregeln, allgemeine Heuristiken wie \textit{Nielsen's Zehn Heuristiken:}
\begin{enumerate}
	\item Sichtbarkeit des Systemstatus
	\item Übereinstimmung zwischen System und der realen Welt
	\item Benutzerkontrolle und Freiheit
	\item Konsistenz und Standards
	\item Fehlervermeidung
	\item Erkennen statt Erinnern
	\item Flexibilität und Effektive Nutzung
	\item Ästhetik und minimalistisches Design
	\item Unterstützung für den Benutzer um Fehler zu erkennen, verstehen und beheben
	\item Hilfe und Dokumentation
\end{enumerate}
\cite[S. 324 - 326]{DixHCI}

\subsection{Ergebnisse}
Die Entwürfe wurden generell positiv aufgenommen. Allerdings wurden auch elementare Probleme festgestellt, welche für den finalen Prototypen geändert werden müssen.\\
Den Evaluatoren war speziell in der Geführten Messung nicht klar, wie es weiter gehen soll. Dies wurde nur über die Navigation realisiert, was in der Optimierten Oberfläche sinnvoll ist, aber für einen geführten Ablauf zu wenig ist. Zwar wurde die Navigation teilweise über Bedienelementen in den Dialogen realisiert, allerdings waren sie nicht eindeutig beschriftet und Auswirkungen waren nicht immer klar.\\
Die Anmeldung entspricht nur einem rudimentären Konzept, die finale Version hängt auch vom Konzept der Benutzerverwaltung in CALIGO ab. Die einfache Übersicht wurde aber positiv bewertet. Kritik gab es bei der Anmeldung eines unbekannten Benutzers: für die Frage nach dem Programmmodus, was dem gewählten Bedienkonzept entspricht, wurde vorgeschlagen keinen neuen Dialog zu verwenden, sondern die Auswahl direkt unterhalb der Eingabemaske zu platzieren. Die Elemente sollten auch besser aufgeteilt werden, damit die Zuordnung von Text und Button leichter erfassbar wird. Dazu wurde vorgeschlagen, eine vertikale Trennung zwischen in der Auswahl zu implementieren.\\
Ein weiterer Punkt ist die Unterscheidung zwischen einer Manuellen Messung und der Auswahl von Prüfplänen. Beide Punkte wurden in der Navigation untereinander platziert, aber es handelt sich um eine Entscheidung zwischen den beiden. Da bisher die Navigation linear von oben nach unten ``abgearbeitet'' wurde, was auch als sehr sinnvoll erachtet wurde, bricht das Vorgehen an dieser Stelle mit dem Konzept. Als Alternative wurde vorgeschlagen, einen weiteren Bildschirm mit einer Auswahlmöglichkeit zu erstellen, der als eine Weiche dient.\\
Weitere Vorschläge gab es zu der Einfärbung der Messelemente je nach Messergebnis. Die Idee wurde generell begrüßt (auch da es in \gls{calypso} ähnlich gelöst wurde), allerdings wurde die Umsetzung bemängelt: durch die starke Einfärbung wird die Ebenenfarbe nur sehr schwer erkennbar, wodurch diese Hilfe zur Orientierung im Prüfplan nicht mehr besteht. %###
 Zudem wird der Bildschirm durch die starke Farbänderung unruhig. Als Alternative wurde vorgeschlagen, statt das komplette Elemente zu färben, einen Bereich darauf wie eine Leuchte zu reservieren, der sich je nach Ergebnis anders färbt. Durch die Wirkung der Farben sollte ein relativ kleiner Bereich dafür ausreichen.\\
Im Prüfplan wurde weiter angemerkt, dass besser ersichtlich sein sollte, welchen Merkmalen bereits Sollwerte und Toleranzen zugewiesen wurden. In kleinen Prüfplänen ist das noch kein Problem, bei umfangreichen wird der Benutzer aber den Überblick verlieren. Zudem sollte im Dialog zur Eingabe für Sollwerte und Toleranzen ersichtlich sein, wenn Änderungen gemacht wurden. Da die Änderungen erst nach einem klick oder tip auf ``übernehmen'' gespeichert werden, führt dies ansonsten zu einer hohen kognitiven Belastung für den Benutzer, die über eine einfache Anzeige vermeidbar ist.\\
In der Optimierten Oberfläche wurde die Zusammenlegung vom Hinzufügen von Ausrichtungen und Merkmalen als negativ empfunden, da der Dialog dadurch überladen wirkt. Durch die vielen Tabs haben sich zwei Reihen gebildet, welche automatisch umsortiert werden, je nach Auswahl. Der gerade aktive Tab wird zusammen mit den anderen seiner Reihe immer unten dargestellt, die weiteren Reihen entsprechend sortiert. Dieses Verhalt könnte Verwirrung stiften und die Orientierung innerhalb des Dialogs erschweren. Da bei einer Messung normalerweise nur eine Ausrichtung vorgenommen wird, stört die Trennung, wie sie in der Geführten Messung vorgenommen wird, nicht und sollte auch hier übernommen werden.\\
Kritik gab es am Bildschirm zur Erstellung von Protokollen. Die Bedienelemente für das Umschalten der Seitenansicht sollten unterhalb dieser platziert werden, da sie zusammen gehören und dies bisher nicht klar ersichtlich ist. Außerdem sollte die Platzierung der Elementen im Einstellungs-Bereich überarbeitet werden, da die Zusammengehörigkeit im Falle der Druck- und Exportoptionen nicht klar ersichtlich ist. Die Größe und Abstand der Bedienelemente, speziell der Checkboxen, wird als zu klein angesehen. Als Alternative würde eine ToggleButton vorgeschlagen, der mit einer kleinen ``Leuchte'' versehen ist. Dies sollte gut erkennbar und zu treffen sein. Um einfacher Merkmale zum Prüfplan hinzufügen zu können sollte ein weiteres Bedienelement eingefügt werden, mit welchem alle Merkmale des Prüfplans außer den Ausrichtung markiert werden.\\
\\
Weitere Punkte sind die teils uneinheitliche Benennung von Elementen, was gerade bei einer Software für unerfahrene Benutzer vermieden werden sollte. Zudem sollte die Suchfunktion den bestehenden Lösungen auf \glspl{tablet} und Mobiltelefonen mit Touchscreens angenähert werden. Optionale Eingaben sollten besser als solche erkennbar sein. Für die Eingabe der Anzahl der Antastpunkte wurde vorgeschlagen, sie durch eine ComboBox mit Vorschlägen zu ersetzten, welche guten Erfahrungswerten entsprechen. Speziell unerfahrene Bediener kennen zwar ein subjektives Maß für die Genauigkeit der Messung (genau/mittel/grob), allerdings wissen sie nicht, wie viele Punkte dafür benötigt werden. Die individuelle Eingabe sollte weiter möglich sein.\\
Zur besseren Unterscheidung der Kategorie-Symbole und den Unterpunkten in der Navigation wurde vorgeschlagen, für Erstere statt eines Icons Text zu verwenden. Zudem wurde der Button zum Abmelden sowie das Zeiss-Logo als zu groß und in der Navigation falsch platziert empfunden. Ersteres kann in einem weiteren Dialog platziert werden, wie es zum Beispiel in Windows gelöst wurde. Das Logo kann auch anderer Stelle weniger prominent platziert werden.\\
\\
Besonders positiv wurden die Navigation mit ihrem klaren Konzept und Ablauf angemerkt, welche einem Messablauf entspricht und dem Benutzer eine gute Orientierung bietet, wo er sich gerade befindet. Die Schritte bauen logisch aufeinander auf und sollten so helfen, einem unerfahrenen Benutzer mehr Sicherheit im Umgang mit der Software zu geben. Auch das Konzept für den Prüfplan wurde gelobt. Die Baumstruktur ist bereits aus anderen Applikationen bekannt und durch die Färbung der Elemente je nach Tiefe kann sich der Benutzer in großen Prüfplänen leichter orientieren, da allein das Einrücken nicht immer ausreichend ist. Die Elemente selber sind groß genug für einen Touchscreen-Bedienung und bieten gleichzeitig viele Informationen auf einen Blick.\\
\\
Aus den Ergebnissen der Evaluation wurde eine priorisierte Liste mit Änderungen erstellt. Die dabei als am wichtigsten eingestuften Anmerkungen wurden noch vor der internen Evaluation und der Abgabe umgesetzt, die weiteren wurden auf einen späteren Zeitpunkt verschoben.\\
Für die umgesetzten Punkte siehe Abschnitt \ref{changes_expert}, für alle weiteren siehe Abschnitt \ref{further_eval}.

\subsection{Änderungen für die Interne Evaluierung}
\label{changes_expert}
Für die interne Evaluierung wurden bereits die elementarsten und einige kleinere Punkte geändert. Die Navigation durch die Bildschirme ist nun nicht mehr nur durch die Navigationsleiste möglich, sondern auch durch die Buttons in den Dialogen. Die Beschriftung wurde entsprechend angepasst, damit dem Benutzer klar ist, mit welchem Schritt es weiter geht. Dies gilt für die Geführte Messung, in der Optimierten Oberfläche wurde dies nur übernommen, wenn es sinnvoll erschien. In diesem Bedienkonzept wird davon ausgegangen, dass der Benutzer weiß was er als nächstes machen will und braucht dadurch keine explizite Führung. Ebenfalls nur in der geführten Messung wurde für die Auswahl ob manuelle Messung oder vorgefertigter Prüfplan ein weitere Bildschirm mit der Auswahl eingefügt.\\
In der Navigation wurden für die Kategorie-Buttons statt Icons Text eingefügt. Der Abmelde-Button und das Zeiss-Logo wurden aus der Navigation entfernt: das Logo wurde kleiner in der rechten oberen Ecke des Bildschirms platziert, neben der Hilfe und dem Einstellungsmenü. Die Abmeldung ist nun über einen weiteren Dialog über den Einstellungs-Button erreichbar. Siehe dazu Abbildung \ref{fig:eval1_auswertung}.\\
Für die Darstellung der Messergebnisse im Prüfplan wurden drei Varianten entworfen, welche nochmals nach Abgabe der Arbeit evaluiert werden. Varianten 1 (Abbildung \ref{fig:eval1_pruefplan1}) entspricht der bisherigen, Varianten 2 (Abbildung \ref{fig:eval1_pruefplan2}) entspricht dem Vorschlag von Intuity, eine vertikal ausgerichtete Anzeige zu verwenden. Die dritte Varianten (Abbildung \ref{fig:eval1_pruefplan3}) ähnelt der zweiten, allerdings wird die Anzeige horizontal in der Zeile des Namens des Elemente angeordnet. Dadurch bleibt mehr Platz für Messergebnisse, da der Namen im Normalfall kürzer ist.\\
Im Details-Dialog wurde für die Anzahl der Antastpunkte eine ComboBox eingefügt, die mit einigen Werten vorbelegt ist. Für einen eigenen Wert steht auch eine Option zur Verfügung, die einen weiteren Dialog zur Eingabe öffnet und anschließend übernimmt. Die Anzeige von Sollwerten und Toleranzen richtet sich nach dem ausgewählten Merkmal, zum Beispiel werden für einen Kreis die Werte des Mittelpunkts als X/Y/Z-Koordinate sowie der Durchmesser angezeigt, während bei einer Distanz die Werte in X-, Y- und Z-Richtung sowie die euklidische Distanz angezeigt. Siehe dazu Abbildung \ref{fig:eval1_details}.\\
Bislang war der Prüfplan immer bereits gefüllt, um dessen Funktionalität zu zeigen. Für die interne Evaluierung ist dies nicht praktikabel, weshalb der Prüfplan nur mit einem Hinweise versehen wurde, dass neue Merkmale hinzugefügt werden müssen. Der Dialog  dafür wurde überarbeitet: das gewählte Element wird mit einem Schlagschatten hervorgehoben. In der Geführten Messung und der Optimierten Oberfläche sind beide nun identisch, da in letzterer der Dialog für Ausrichtungen herausgelöst wurde. Siehe dazu Abbildung \ref{fig:eval1_pruefplan-neu}.\\
In der Auswertung wurden die Bedienelemente für die Seitenvorschau unter diese verlegt. Dadurch wird in der Einstellungsspalte mehr Platz für weitere Parameter oder die bessere Platzierung dieser frei. Siehe dazu Abbildung \ref{fig:eval1_auswertung}.\\

\begin{figure}[htbp]
  \centering
  \subfigure[Leerer Prüfplan und neues Merkmal-Dialog]{
    \label{fig:eval1_pruefplan-neu}
    \includegraphics[width=0.5\textwidth]{gfx/eval1_pruefplan-neu.png}
  }~
  \subfigure[Details-Dialog]{
    \label{fig:eval1_details}
    \includegraphics[width=0.35\textwidth]{gfx/eval1_details.png}
  }
  \caption{Änderungen für interne Evaluation}
  \label{fig:eval1}
\end{figure}

% je nach Layout drehen
\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{gfx/eval1_auswertung.png}
	\caption{Neue Auswertung und Anordnung in Navigation}
	\label{fig:eval1_auswertung}
\end{figure}

\begin{figure}[htbp]
  \centering
  \subfigure[Varianten 1]{
    \label{fig:eval1_pruefplan1}
    \includegraphics[width=0.3\textwidth]{gfx/eval1_pruefplan1.png}
  }~
  \subfigure[Variante 2]{
    \label{fig:eval1_pruefplan2}
    \includegraphics[width=0.3\textwidth]{gfx/eval1_pruefplan2.png}
  }
  \subfigure[Variante 3]{
    \label{fig:eval1_pruefplan3}
    \includegraphics[width=0.3\textwidth]{gfx/eval1_pruefplan3.png}
  }
  \caption{Entwürfe für Darstellung der Messergebnisse im Prüfplan}
  \label{fig:eval1_pruefplan}
\end{figure}


\section{Interne Evaluierung}
\label{eval_internal}
Ein weitere, interne Evaluierung wurde als \textit{Kontrolliertes Experiment} durchgeführt. Dabei bekommen die Teilnehmer eine im Werkstattbereich typische Aufgabe gestellt, die mit einem Prototyp der Software erledigen sollen. Das Experiment wird dabei in einer kontrollierten Umgebung durchgeführt und das Vorgehen und Reaktionen der Benutzer dokumentiert. Dies ist sehr aufwändig, aber die Ergebnisse sind, bei geeigneter Aufgabenstellung, sehr genau. Zudem können Kriterien wie die Geschwindigkeit, Fehlerbehandlung und Zeit, die die Benutzer benötigten um sich in das System einzufinden, gemessen und protokolliert.\\
Im Experiment wurden neben schriftlichen Protokollen mit einer Videokamera gearbeitet. Damit wurden die Bewegungen auf dem Bildschirm sowie das sogenannte ``think aloud''-Protokoll festgehalten. Die Tester wurden dazu vor Beginn des Tests dazu angehalten, ihre Gedanken laut auszusprechen, um ihre Gedankengänge nachvollziehen zu können.\\
\cite[S. 216]{HerczegSwErgo}

\subsection{Teilnehmer}
Der Teilnehmer war ein Mitarbeiter der \gls{imt} aus dem Bereich der Qualitätssicherung für Messsoftware. Er verfügte über gute Kenntnisse in der Messtechnik, aber sehr unterschiedliche Kenntnisse im Bezug auf das Werkstattumfeld und keine Vorkenntnisse über den hier entwickelten Bedienkonzept und Prototyp.

\subsection{Ergebnisse}
Wie erwartet hat der Tester zuerst die Oberfläche erkundet und hat dabei ausgiebigen Gebrauch von den Tooltips gemacht. Die Funktionsweise der Navigationsleiste wurde nicht sofort erkannt, da alle Icons gleich groß sind und ihr Zusammenhang nicht sofort klar wurde. Die Farbe hat er zuerst nicht beachtet, erst im Verlauf wurde klar, welche Icons zusammengehören und eine Untergruppe bilden. In diesem Zusammenhang wurde angemerkt, die Unterpunkte kleiner und leicht eingerückt darzustellen, ähnlich einer Baumstruktur, um die Kategorie- und von Unterpunkten besser unterscheiden zu können.\\
In den Messschritten war dem Tester nicht immer klar, an welcher Stelle er sich befindet. Dies ist teilweise auf den fehlenden Schlagschatten-Effekt in der Navigation zurückzuführen, der von Sketchflow beim Wechseln eines Bildschirms nicht angezeigt wird, obwohl das passende Event dafür definiert wurde. Die Führung durch den Ablauf wurde als positiv und sinnvoll empfunden. In der Konfiguration für die manuelle Messung kam es zu einer Überschneidung der Begriffe: In der Auswahl eines CAD-Modells wurde es schlicht als Modell bezeichnet, was in anderen Softwarepaketen auch für einen vorgefertigten Prüfplan stehen kann. Für weitere Prototypen sollten die Begriffen auf eventuelle Überschneidungen geprüft werden oder feste Begriffe definiert werden.\\
Der Button unterhalb des Prüfplans um einen Dialog zu öffnen wurde zuerst übersehen, erst nach längerem Studium des Bildschirms wurde er entdeckt und sein nutzen schnell erkannt. Nach kurzer Eingewöhnung wurde er schnell wieder gefunden und wie gedacht angewendet. Das Erstellen von Merkmalen wurde schnell begriffen, das Einstellen der Toleranz werden wurde nur durch Suchen und probieren gefunden. Hier sollte der Button besser beschriftet werden. Bei der Eingabe der Toleranzen waren die Buttons zum Schließen und Übernehmen des kleinen Dialogs zwar gesehen, aber da deren Nutzen nicht erkannt wurde, wurde per Klick auf die anderen Felder gewechselt. Da nur der Dialog des zuletzt gewählten Wertes geöffnet ist, wird der vorherige geschlossen. Hier sollten statt des X und Ü eindeutigere Symbole verwendet werden, als Beispiele aus anderen Softwarepaketen wurde ein rotes X und ein grüner Haken genannt. Zusätzlich sollte auch eine Bestätigung der Eingabe per Enter-Taste eingebaut werden, da entsprechende Werte oft über den Nummernblock einer Tastatur eingegeben werden und es damit schneller geht. Bei der Anzeige wurde angemerkt, dass die Toleranz nur als ein Wert dargestellt werden kann, wenn die obere und untere Grenze symmetrisch ist. Andernfalls müssen beide Werte dargestellt werden.\\
In der Messung wurde kritisiert, dass bei der Anzeige des Ergebnisses nicht klar ist, was die jeweiligen Angaben im Merkmal bedeuten. Hier muss eine Kennzeichnung erfolgen, ein kurzer Text davor (zum Beispiel ``Soll: ...'') würde schon reichen. In der Auswertung sollte ein Button eingefügt werden, der die hinzugefügten Merkmale zurücksetzt. Die Bedienelemente zum Drucken, Exportieren und Übertrag in eine Datenbank wurden sofort zugeordnet und die zusammengehörigen Merkmale erkannt.

\subsection{Umgesetzte Änderungen}
\label{changes_internal}
Von der Ergebnisses der internen Evaluierung wurden im Rahmen dieser Arbeit nur die wichtigsten Änderungen umgesetzt.\\
Die erste betrifft die Anzeige der Toleranzwerte. Es werden beide Werte übereinander dargestellt. Diese Ansicht wird bereits in \gls{holos} verwendet, nur wenn die Grenzen symmetrisch sind wird ein zusammengezogener Wert dargestellt. Siehe dazu Abbildung \ref{fig:eval2_details}.\\
Im Prüfplan wurde eine kleine Änderung vorgenommen: Bei der Anzeige der Messergebnisse wird vor den Ergebnissen mit ``Soll:'' und ``Ist:'' klar dargestellt, um welche Werte es sich handelt.  Siehe Abbildung \ref{fig:eval2_messergebnis}.

\begin{figure}[htbp]
  \centering
  \subfigure[Details-Dialog]{
    \label{fig:eval2_details}
    \includegraphics[width=0.5\textwidth]{gfx/eval2_details.png}
  }~
  \subfigure[Messergebnis]{
    \label{fig:eval2_messergebnis}
    \includegraphics[width=0.35\textwidth]{gfx/eval1_pruefplan3.png}
  }
  \caption{Änderungen durch die interne Evaluierung}
  \label{fig:eval2}
\end{figure}

\section{Gesamtfazit}
Das entwickelte Konzept wurde in allen Evaluationen grundsätzlich positiv aufgenommen. In fanden sich zwar Probleme und Unstimmigkeiten, allerdings war keines von so grundlegender Natur, dass das Konzept komplett geändert werden musste. Vielmehr handelte es sich um bisher nicht beachtete oder übersehene Probleme, die erst durch das Einbeziehen weiterer Personen auffielen.\\
Die schwersten Probleme wurden zwischen den Evaluationen korrigiert. Weitere Probleme mit klarere Lösung werden nach Abschluss dieser Arbeit behandelt. Einige der Probleme haben neue Fragen aufgeworfen oder es besteht noch Diskussionsbedarf, weshalb eine klare Lösung nicht in naher Zukunft zu erwarten ist. Sie werden zu einem späteren Zeitpunkt behandelt. Siehe dazu Abschnitt \ref{further_eval}.
