@misc{id,
	author		= "",
	title		= "",
	howpublished = "\url{}",
	note		= "Zuletzt aufgerufen: YYYY/MM/DD"
}

@article{id
    author    = "",
    title     = "{}",
    journal   = "{}",
    month   = "", %% als Ganzzahl
    pages    = "", %% von -- bis
    year      = "" %% YYYY
}

@book{id,
    author    = "",
    title     = "",
    year      = "",
	edition   = "",
    publisher = ""
}

@mastersthesis{id,
    author    = "",
    title     = "",
    school    = "",
    type      = "", % Freitext
    year      = ""
}
